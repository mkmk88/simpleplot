#!/bin/bash

DATA_FILE="data.txt"
CMD="cat /proc/meminfo | grep Dirty"
SLEEPRATE=0.01

while(true)
do
    adb shell $CMD | awk '{ if ( $1 ~ "Dirty:" ) { print $2 } }' >> $DATA_FILE
    sleep $SLEEPRATE
done

