#! /usr/bin/env python
import Gnuplot, Gnuplot.funcutils
from time import sleep
import sys 

def plot():
    ticks = 100;
    sleeprate = 0.1;
    g = Gnuplot.Gnuplot(debug=0)
    g('set ylabel "kB"');
    g('set xlabel "Ticks"');
    g('set yrange [0:8000]');
    g('set xrange [1:' + str(ticks) + ']');
    g('set ytics 500');
    g.title('<title>')
    cmd = "plot \"< tail -n" + str(ticks) + " " + sys.argv[1] + "\" with lines title '[xx]'"
    g(cmd)
    while True:
        sleep(sleeprate);
        g('reread');
        g('replot');

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Data file is required"
    else:
        plot()

